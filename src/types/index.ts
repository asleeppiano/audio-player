import type { MusicPlayerState } from '@/types/enums';

export type Track = {
    artist: string;
    title: string;
    duration: number;
    file: string;
};

export type MusicPlayerStoreState = {
    currentTrack: Track | null;
    audioElement: HTMLAudioElement | null;
    currentTime: number;
    volume: number;
    state: MusicPlayerState;
};

export type TrackState = {
    trackList: Array<Track>;
    search: string;
};
