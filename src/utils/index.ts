export const clamp = (num: number, min: number, max: number) =>
    Math.min(Math.max(min, num), max);

export function formatTime(time: number): string {
    const mins = Math.floor(time / 60);
    const secs = Math.floor(time % 60);
    return `${mins > 9 ? mins : `0${mins}`}:${secs > 9 ? secs : `0${secs}`}`;
}

export function isMouseEvent(e: Event): e is MouseEvent {
    return (e as MouseEvent).pageX !== undefined;
}

export function isTouchEvent(e: Event): e is MouseEvent {
    return (e as TouchEvent).changedTouches !== undefined;
}
