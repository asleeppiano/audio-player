import { defineStore } from 'pinia';
import type { MusicPlayerStoreState } from '@/types';
import { MusicPlayerState } from '@/types/enums';

export const useMusicPlayerStore = defineStore({
    id: 'musicPlayer',
    state: (): MusicPlayerStoreState => ({
        currentTrack: null,
        audioElement: null,
        currentTime: 0,
        volume: 70,
        state: MusicPlayerState.STOPPED,
    }),
    getters: {
        duration: (state) => state.currentTrack?.duration ?? 0,
        fileUrl: (state) =>
            `${import.meta.env.BASE_URL}assets/${state.currentTrack?.file}`,
    },
    actions: {
        toggleAudio() {
            if (this.audioElement) {
                if (this.audioElement.paused) {
                    this.playAudio();
                } else {
                    this.stopAudio();
                }
            }
        },
        playAudio() {
            if (this.audioElement) {
                this.audioElement.play();
                this.state = MusicPlayerState.PLAYING;
            }
        },
        stopAudio() {
            if (this.audioElement) {
                this.audioElement.pause();
                this.state = MusicPlayerState.STOPPED;
            }
        },
    },
});
