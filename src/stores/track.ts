import { defineStore } from 'pinia';
import type { TrackState } from '@/types';
import { useMusicPlayerStore } from './musicPlayer';

export const useTrackStore = defineStore({
    id: 'track',
    state: (): TrackState => ({
        trackList: [],
        search: '',
    }),
    getters: {
        searchedTracks: (state) =>
            state.search !== ''
                ? state.trackList.filter((track) => {
                      const regex = new RegExp(state.search, 'i');
                      return (
                          track.title.search(regex) !== -1 ||
                          track.artist.search(regex) !== -1
                      );
                  })
                : state.trackList,
    },
    actions: {
        async loadTracksMeta() {
            try {
                const res = await fetch(
                    `${import.meta.env.BASE_URL}musiclist.json`
                );
                const json = await res.json();
                this.trackList = json;
                const musicPlayerStore = useMusicPlayerStore();
                musicPlayerStore.currentTrack = this.trackList[0];
            } catch {
                // ignore
            }
        },
        setTrackByDirection(dir: 'next' | 'prev') {
            const musicPlayerStore = useMusicPlayerStore();
            const trackIndex = this.searchedTracks.findIndex(
                (track) => track.file === musicPlayerStore.currentTrack?.file
            );
            const index = dir === 'next' ? trackIndex + 1 : trackIndex - 1;
            musicPlayerStore.currentTrack = this.searchedTracks[index]
                ? this.searchedTracks[index]
                : this.searchedTracks[
                      dir === 'next' ? 0 : this.searchedTracks.length - 1
                  ];
        },
    },
});
