# audio-player

Audio player

[demo](https://asleeppiano.gitlab.io/audio-player/)

## Local dev

``` sh
git clone git@gitlab.com:asleeppiano/audio-player.git
cd audio-player
npm i
npm run dev
```

## Music meta

To get info about music, generate `musiclist.json` file. It will generate a list of mp3 music located in `public/assets` directory.

``` sh
npm run genMusicMeta
```
