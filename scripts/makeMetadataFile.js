/* eslint-env node */
const mm = require('music-metadata');
const fs = require('fs');
const path = require('path');

const PUBLIC_DIR = path.resolve(__dirname, '../public');
const DIR_NAME = path.resolve(PUBLIC_DIR, 'assets');

let meta = [];

async function main() {
    try {
        const dir = fs.readdirSync(DIR_NAME);
        for (let filename of dir) {
            if (path.extname(filename) === '.mp3') {
                const metadata = await mm.parseFile(
                    path.resolve(DIR_NAME, filename)
                );
                meta.push({
                    title: metadata.common.title,
                    artist: metadata.common.artist,
                    duration: metadata.format.duration,
                    file: filename,
                });
            }
        }
        fs.writeFileSync(
            path.join(PUBLIC_DIR, 'musiclist.json'),
            JSON.stringify(meta)
        );
    } catch (error) {
        console.error(error.message);
    }
}

main();
